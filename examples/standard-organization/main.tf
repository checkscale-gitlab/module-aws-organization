provider "aws" {
  region = "ca-central-1"
  # tfsec:ignore:AWS044
  access_key = var.access_key
  # tfsec:ignore:GEN003
  secret_key = var.secret_key

  # NOTE: Should only be used for testing unless you know what you're doing
  skip_credentials_validation = true
  skip_get_ec2_platforms      = true
  skip_region_validation      = true
  skip_requesting_account_id  = true
  skip_metadata_api_check     = true
}

module "organization" {
  source = "../../"

  accounts = {
    dev = {
      email = "dev@fake.com"
    }
    int = {
      email     = "int@fake.com"
      role_name = "int-root-role"
      tags      = { fake = "true" }
    }
    prd = {
      email     = "prd@fake.com"
      parent_id = module.organization.organizational_unit_ids.ops
    }
  }

  organizational_units = {
    ops = {}
    dev = {}
  }
}

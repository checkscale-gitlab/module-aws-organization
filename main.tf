#####
# Locals
#####

locals {
  tags = {
    managed-by = "terraform"
  }
  organizational_units_defaults = {
    parent_id = aws_organizations_organization.this.id
  }
  organizational_units = {
    for key, value in var.organizational_units : key => defaults(value, local.organizational_units_defaults)
  }
  account_defaults = {
    iam_user_access_to_billing = "ALLOW"
    parent_id                  = aws_organizations_organization.this.id
  }
  accounts = {
    for key, value in var.accounts : key => defaults(value, local.account_defaults)
  }
}

#####
# Organization
#####

resource "aws_organizations_organization" "this" {
  aws_service_access_principals = var.aws_service_access_principals
  enabled_policy_types          = var.enabled_policy_types
  feature_set                   = var.feature_set
}

#####
# Organizational Units
#####

resource "aws_organizations_organizational_unit" "this" {
  for_each = local.organizational_units

  name      = each.key
  parent_id = each.value.parent_id
  tags      = merge(local.tags, var.organizational_unit_tags, lookup(each.value, "tags", {}))
}

#####
# Accounts
#####

resource "aws_organizations_account" "this" {
  for_each = local.accounts

  name                       = each.key
  email                      = each.value.email
  iam_user_access_to_billing = each.value.iam_user_access_to_billing
  parent_id                  = each.value.parent_id
  role_name                  = lookup(each.value, "role_name", null)
  tags                       = merge(local.tags, var.account_tags, lookup(each.value, "tags", {}))
}
